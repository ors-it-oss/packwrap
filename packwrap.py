#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Python
import os
import sys
import argparse

# Logging & Debugging
import logging
import pprint

# Own
import packwrap

pp = pprint.PrettyPrinter(indent=2)


def parse_args():
    pw_enhanced = {
        'build': 'Build Overrides',
        'inspect': 'Inspection overrides',
        'validate': 'Validation overrides'
    }

    parser = argparse.ArgumentParser(
        prog='PackWrap',
        description='Your handy-dandy Pythonic Packer Wrapping buddy'
    )
    parser.add_argument('-v', '--version', help='Show version', action='store_true')

    command_parent = argparse.ArgumentParser(add_help=False)
    command_parent.add_argument('-var-file', dest='var_files', default=[], action='append',
                                help='Var file. Can be given multiple times')
    command_parent.add_argument('-path', dest='path', default=os.getcwd(),
                                help='Active directory during Packer execution '
                                     '(no impact on template rendering/finding!')
    command_parent.add_argument('-var', dest='vars', action='append', default=[],
                                help='key=value; merged in temp var file')
    command_parent.add_argument('templates', nargs='+', default=[])

    commands = parser.add_subparsers(dest='command', help='Commands')
    commands.required = True

    for command, cmd_help in pw_enhanced.items():
        commands.add_parser(command, parents=[command_parent], help=cmd_help)

    command_parent = argparse.ArgumentParser(add_help=False)
    # command_parent.add_argument('args', nargs='*')

    for command in ('fix', 'push', 'version'):
        commands.add_parser(command, parents=[command_parent], help='Ignored & passed on')

    args, xargs = parser.parse_known_args()

    # pp.pprint(args)

    if args.version:
        print(packwrap.version())
        sys.exit(0)

    pw_args = {
        'command': args.command,
        'args': xargs,
        'path': args.path
    }

    if args.command in pw_enhanced:
        pw_args.update({
            'var_files': args.var_files,
            'templates': args.templates,
            'user_vars': dict(
                [kv.split('=', 1) for kv in args.vars]
                )
        })

    # pp.pprint(pw_args)

    return pw_args


if __name__ == '__main__':

    packwrap.utils.set_logger()
    log = logging.getLogger('PackWrap')

    args = parse_args()

    log.debug('Launching PackWrapper with {}'.format(args))
    packwrap.packer(
        **args
    )
